﻿using System.CodeDom;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;

namespace XmlSchemaClassGenerator.SourceGenerator;

/// <inheritdoc />
/// <inheritdoc />
internal class ContextOutputWriter(SourceProductionContext context) : OutputWriter
{
    /// <inheritdoc />
    public override void Write(CodeNamespace cn)
    {
        var cu = new CodeCompileUnit();
        cu.Namespaces.Add(cn);

        using var stream = new MemoryStream();
        using (var writer = new StreamWriter(stream, Encoding.UTF8, 1024, true))
        {
            Write(writer, cu);
        }

        var content = SourceText.From(stream, canBeEmbedded: true);
        context.AddSource($"{cn.Name}.g.cs", content);
    }
}