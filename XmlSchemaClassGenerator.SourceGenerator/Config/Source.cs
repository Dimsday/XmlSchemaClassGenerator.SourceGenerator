﻿namespace XmlSchemaClassGenerator.SourceGenerator.Config;

/// <summary>
///     Source
/// </summary>
// ReSharper disable once ClassNeverInstantiated.Global
public class Source
{
    /// <summary>
    ///     Namespaces
    /// </summary>
    public Dictionary<string, string> Namespaces { get; set; } = null!;

    /// <summary>
    ///     Path
    /// </summary>
    public string? Path { get; set; }
}