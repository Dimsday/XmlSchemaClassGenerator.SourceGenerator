﻿namespace XmlSchemaClassGenerator.SourceGenerator.Config;

/// <summary>
///     Config root
/// </summary>
public class Root
{
    /// <summary>
    ///     Collection setters mode
    /// </summary>
    public CollectionSettersMode CollectionSettersMode { get; set; }

    /// <summary>
    ///     Compact type names
    /// </summary>
    public bool CompactTypeNames { get; set; }

    /// <summary>
    ///     Enable nullable reference attributes
    /// </summary>
    public bool EnableNullableReferenceAttributes { get; set; }

    /// <summary>
    ///     Generate nullables
    /// </summary>
    public bool GenerateNullables { get; set; }

    /// <summary>
    ///     .NET specific code
    /// </summary>
    public bool NetCoreSpecificCode { get; set; }

    /// <summary>
    ///     Sources
    /// </summary>
    public Source[]? Sources { get; set; }
}