﻿using System.Collections.Immutable;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using XmlSchemaClassGenerator.SourceGenerator.Config;

namespace XmlSchemaClassGenerator.SourceGenerator;

/// <inheritdoc />
[Generator]
public class XscGenGenerator : IIncrementalGenerator
{
    /// <inheritdoc />
    public void Initialize(IncrementalGeneratorInitializationContext context)
    {
        var source = context.AdditionalTextsProvider
            .Where(text => text.Path.EndsWith(".json") || text.Path.EndsWith(".xsd")).Collect()
            .Combine(context.AnalyzerConfigOptionsProvider);

        context.RegisterSourceOutput(source, Generate);
    }

    private static void Generate(
        SourceProductionContext context,
        (ImmutableArray<AdditionalText> Files, AnalyzerConfigOptionsProvider Options) source
    )
    {
        var files = new List<string>();
        var configs = new List<AdditionalText>();

        foreach (var file in source.Files)
        {
            var options = source.Options.GetOptions(file);
            if (options.TryGetValue("build_metadata.AdditionalFiles.XscGenSchema", out var isSchema) &&
                isSchema == "true")
            {
                files.Add(file.Path);
            }

            if (options.TryGetValue("build_metadata.AdditionalFiles.XscGenConfig", out var isMapping) &&
                isMapping == "true")
            {
                configs.Add(file);
            }
        }

        var writer = new ContextOutputWriter(context);

        var generator = new Generator
        {
            Configuration = { OutputWriter = writer }
        };

        var configFile = configs.LastOrDefault();
        var text = configFile?.GetText();

        if (text != null)
        {
            var config = JsonSerializer.Deserialize<Root>(text.ToString(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                    Converters = { new JsonStringEnumConverter(JsonNamingPolicy.CamelCase) }
                });

            if (config != null)
            {
                generator.GenerateNullables = config.GenerateNullables;
                generator.CompactTypeNames = config.CompactTypeNames;
                generator.NetCoreSpecificCode = config.NetCoreSpecificCode;
                generator.EnableNullableReferenceAttributes = config.EnableNullableReferenceAttributes;
                generator.CollectionSettersMode = config.CollectionSettersMode;

                if (config.Sources?.Any() == true)
                {
                    foreach (var configSource in config.Sources)
                    {
                        if (!configSource.Namespaces.Any())
                        {
                            continue;
                        }

                        foreach (var ns in configSource.Namespaces)
                        {
                            var uri = configSource.Path != null ? new Uri(configSource.Path) : null;
                            generator.NamespaceProvider[new NamespaceKey(uri, ns.Key)] = ns.Value;
                        }
                    }
                }
            }
        }

        generator.Generate(files);
    }
}