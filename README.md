## XmlSchemaClassGenerator.SourceGenerator

Source generator for [XmlSchemaClassGenerator](https://github.com/mganss/XmlSchemaClassGenerator/tree/master).

### Example `.csproj` file modification

```git
<Project Sdk="Microsoft.NET.Sdk">

	<PropertyGroup>
		<TargetFramework>net8.0</TargetFramework>
		<ImplicitUsings>enable</ImplicitUsings>
		<Nullable>enable</Nullable>
	</PropertyGroup>

+ 	<ItemGroup>
+		<PackageReference Include="XmlSchemaClassGenerator.SourceGenerator" Version="1.0.0" />
+	</ItemGroup>

+	<ItemGroup>
+		<AdditionalFiles Include="Schemas\**\*.xsd" XscGenSchema="true" />
+		<AdditionalFiles Include="Schemas\Config.json" XscGenConfig="true" />
+	</ItemGroup>

</Project>
```

### Example `Config.json`

**Note**: Used only last config in project

```json
{
  "sources": [
    {
      "path": null,
      "namespaces": {
        "urn://x-artefacts-smev-gov-ru/services/service-adapter/types": "Schemas.Adapter",
        "urn://x-artefacts-smev-gov-ru/services/service-adapter/types/faults": "Schemas.Adapter.Faults"
      }
    }
  ],
  "CollectionSettersMode": "PublicWithoutConstructorInitialization",
  "CompactTypeNames": true,
  "EnableNullableReferenceAttributes": true,
  "GenerateNullables": true,
  "NetCoreSpecificCode": true
}
```